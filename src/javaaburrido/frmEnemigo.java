package javaaburrido;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.Timer;

public class frmEnemigo {
	public int PosX;
	public int PosY;
	
	public int anchoframe;
	public int altoframe;
	public int posxframe;
	public int posyframe;
	
	public int anchomax;
	public int altomax;
	
	public boolean moviendo;
	public int Velocidad;
	
	public int Anim;
	
	private Timer tmrAnim;
	
	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frmEnemigo window = new frmEnemigo();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public frmEnemigo() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		//aqui inicializamos
		anchomax = 104;
		altomax = 188;
		
		altoframe = altomax / 4;
		anchoframe = anchomax / 4;
		
		Anim = 1;
		
		frame = new JFrame();
		frame.setBounds(100, 100, anchoframe,altoframe );
		frame.setUndecorated(true);
		frame.setAlwaysOnTop(true);
		frame.setBackground(new Color(0.0f,0.0f,0.0f,0.0f));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		Image img = new ImageIcon(this.getClass().getResource("/2.png")).getImage();
		JLabel lblpicAnim = new JLabel("picAnim");
		lblpicAnim.setBounds(0, 0, anchomax, altomax);
		lblpicAnim.setIcon(new ImageIcon(img));
		frame.getContentPane().add(lblpicAnim);
		
		tmrAnim = new Timer(150, new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				Anim += 1;
				
				if (Anim > 4) {
					Anim = 1;
					PosX = 0 + anchoframe;
				}
				
				PosX = PosX - anchoframe;
				lblpicAnim.setLocation(PosX, PosY);
			}
			
		});
		
		tmrAnim.start();
	}
}
