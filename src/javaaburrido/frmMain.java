package javaaburrido;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.Timer;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class frmMain {
	public int altoframe;
	public int anchoframe;
	public int posxframe;
	public int posyframe;
	public int altomax;
	public int anchomax;
	public int Anim;
	public boolean moviendo;
	public byte moviendoUp;
	public byte moviendoDown;
	public byte moviendoLeft;
	public byte moviendoRight;
	
	public int Velocidad;
	
	public int PosX;
	public int PosY;
	
	public Timer tmrAnim;
	public Timer tmrCamina;
	public Timer tmrEngine;
	
	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frmMain window = new frmMain();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public frmMain() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		//inicializamos las variables
		anchomax = 92;
		altomax = 176;
		PosX = 500;
		PosY = 300;
		anchoframe = anchomax / 4;
		altoframe = altomax / 4;
		Anim = 1; //de lo contrario parpadea, ya que toma una accion demas si lo dejaba en 0
		Velocidad = 3; //la velocidad con la que se mueve
		
		frame = new JFrame();
		frame.setBounds(PosX, PosY, anchoframe, altoframe);
		frame.setUndecorated(true);
		frame.setAlwaysOnTop(true);
		frame.setBackground(new Color(0.0f,0.0f,0.0f,0.0f));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				int keyCode = e.getKeyCode();
		        if(keyCode == KeyEvent.VK_DOWN) {
		        	moviendoDown = 1;
		        	moviendo = true;
		        }
		        
		        if (keyCode == KeyEvent.VK_UP) {
		        	moviendoUp = 1;
		        	moviendo = true;
		        }
		        
		        if (keyCode == KeyEvent.VK_LEFT) {
		        	moviendoLeft = 1;
		        	moviendo = true;
		        }
		        
		        if (keyCode == KeyEvent.VK_RIGHT) {
		        	moviendoRight = 1;
		        	moviendo = true;
		        }
			}
			
			public void keyReleased(KeyEvent e) {
				int keyCode = e.getKeyCode();
				//switch(KeyCode){
				//case KeyEvent.VK_DOWN:
				//	moviendoDown = 0;
				//break;
				//}
				if (keyCode == KeyEvent.VK_DOWN) {
					moviendoDown = 0;
				}
				
				if (keyCode == KeyEvent.VK_UP) {
		        	moviendoUp = 0;
		        }
		        
		        if (keyCode == KeyEvent.VK_LEFT) {
		        	moviendoLeft = 0;
		        }
		        
		        if (keyCode == KeyEvent.VK_RIGHT) {
		        	moviendoRight = 0;
		        }
		        
		        if (moviendoUp == 0 && moviendoDown == 0 && moviendoLeft == 0 && moviendoRight == 0) {
		        	moviendo = false;
		        }
			}
			
		});
		
		Image img = new ImageIcon(this.getClass().getResource("/1.png")).getImage();
		JLabel lblpicAnim = new JLabel("picAnim");
		lblpicAnim.setBounds(anchoframe, 0, anchomax, altomax);
		lblpicAnim.setIcon(new ImageIcon(img));
		frame.getContentPane().add(lblpicAnim);
		
		//Timer
		tmrAnim = new Timer(150,new ActionListener() {

		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			
			if (moviendo == true) {
				Anim += 1;
				if (Anim > 4) {
					Anim = 1;
					posxframe = 0 + anchoframe;
				}
				
			}
			else {
				Anim = 1;
				posxframe = 0 + anchoframe;
			}
			
			posxframe = posxframe - anchoframe;
			lblpicAnim.setBounds(posxframe, posyframe, anchomax, altomax);
		}
					
		});
		
		tmrAnim.start();
		
		tmrCamina = new Timer(10,new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				if (moviendoDown == 1) {
					PosY += Velocidad;
					frame.setLocation(PosX,PosY );
					posyframe = 0;
				}
				
				if (moviendoUp == 1) {
					PosY -= Velocidad;
					frame.setLocation(PosX,PosY );
					posyframe = 0 - altoframe * 3;
				}
				
				if (moviendoLeft == 1) {
					PosX -= Velocidad;
					frame.setLocation(PosX,PosY );
					posyframe = 0 - altoframe;
				}
				
				if (moviendoRight == 1) {
					PosX += Velocidad;
					frame.setLocation(PosX,PosY );
					posyframe = 0 - altoframe * 2;
				}
			}
			
		});
		
		tmrCamina.start();
		
		tmrEngine = new Timer(10,new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				
			}
			
		});
	}

}
